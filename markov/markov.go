package markov

import (
	// "bytes"
	// "encoding/gob"
	// "github.com/davecgh/go-spew/spew"
	// "ioutil"
	// "os"
	"math/rand"
	"strings"
)

type entry struct {
	first, second string
}

var (
	model = map[entry]map[string]float64{} // want to serialize this

	// Need sum types ugh
	startChar  = "^" // can't be in input text
	endChar    = "$"
	startEntry = entry{startChar, startChar}
)

/*
func WriteModel(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	e := gob.NewEncoder(file)
	if err := e.Encode(model); err != nil {
		return err
	}
	return nil
}

func ReadModel(path string) error {
	content, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}
	d := gob.NewDecoder(bytes.NewBuffer(content))
	if err := d.Decode(model); err != nil {
		return err
	}
	return nil
}
*/
func UpdateModel(text string) {
	text = strings.ReplaceAll(text, endChar, "")
	text = strings.ReplaceAll(text, startChar, "")
	sentences := strings.FieldsFunc(text, testChar)
	for _, v := range sentences {
		fields := strings.Fields(v)
		if len(fields) != 0 {
			incrementValue(startEntry, fields[0])
		}
		prev := startChar
		for i, v := range fields {
			var key string
			if i == len(fields)-1 {
				key = "$"
			} else {
				key = fields[i+1]
			}
			incrementValue(entry{prev, v}, key)
			prev = v
		}
	}
}

func getWeights(m map[string]float64) []float64 {
	r := make([]float64, 0, len(m))
	for _, v := range m {
		r = append(r, v)
	}
	return r
}

func getStrings(m map[string]float64) []string {
	r := make([]string, 0, len(m))
	for k, _ := range m {
		r = append(r, k)
	}
	return r
}

func RandomSentence() string {
	var out string
	current := startEntry
loop:
	for {
		weights := getWeights(model[current])
		strings := getStrings(model[current])
		rnd := weightedRandom(weights)
		if rnd == -1 {
			return ""
		}
		switch strings[rnd] {
		case "^":
			panic("unreachable")
		case "$":
			break loop
		default:
			out += " " + strings[rnd]
			current = entry{current.second, strings[rnd]}
		}
	}

	out += "."
	return out
}

func sum(slice []float64) float64 {
	var accum float64
	for _, v := range slice {
		accum += v
	}
	return accum
}

func weightedRandom(weights []float64) int {
	rnd := rand.Float64() * float64(sum(weights))
	for i, w := range weights {
		rnd -= w
		if rnd < 0 {
			return i
		}
	}
	return -1
}

//  Debug only
// func PrintModel() {
// 	spew.Dump(model)
// }

func incrementValue(key entry, val string) {
	if _, ok := model[key]; !ok {
		model[key] = map[string]float64{}
	}
	if _, ok := model[key][val]; !ok {
		model[key][val] = 1.0
	}
	model[key][val] *= 2.0
}

func testChar(char rune) bool {
	return strings.ContainsRune("!.\n", char)
}
