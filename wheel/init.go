package wheel

import "github.com/asdine/storm"

var (
	db      *storm.DB
	node    storm.Node
	actions []Action
)

// Initialize initializes the logger
func Initialize(d *storm.DB) {
	db = d
	node = db.From("wheel")
}

// AddActions adds actions
func AddActions(as ...Action) {
	actions = append(actions, as...)
}
