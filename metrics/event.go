package metrics

import (
	"github.com/bwmarrin/discordgo"
)

// AddEventFromUser increments the counter for the user
func (m *Metrics) AddEventFromUser(u *discordgo.User, guildID string) {
	if u == nil {
		return
	}

	if guildID == "" {
		return
	}

	m.Lock()
	defer m.Unlock()

	if g, ok := m.framedata[guildID]; ok {
		g[u.ID]++
		return
	}

	m.framedata[guildID] = map[string]uint{
		u.ID: 1,
	}
}
