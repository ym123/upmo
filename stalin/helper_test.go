package stalin

import (
	"testing"
)

func TestWordInSlice(t *testing.T) {
	var w []string

	sl1 := []string{"this", "is", "a", "sl", "ice"}

	w = wordInSlice(
		testGenerateMessages(sl1),
		"slice",
	)

	if !testEqualArray(w, []string{"sl", "ice"}) {
		t.Fatal("Failed stage 1")
	}

	sl2 := []string{"this", "is", "random"}

	w = wordInSlice(
		testGenerateMessages(sl2),
		"slice",
	)

	if !testEqualArray(w, nil) {
		t.Fatal("Failed stage 2")
	}
}

func testGenerateMessages(msgs []string) []*message {
	mg := make([]*message, len(msgs))
	for i, m := range msgs {
		mg[i] = &message{
			ID: m, Content: m,
		}
	}

	return mg
}

func testEqualArray(a1, a2 []string) bool {
	if len(a1) != len(a2) {
		return false
	}

	for i := 0; i < len(a1); i++ {
		if a1[i] != a2[i] {
			return false
		}
	}

	return true
}
