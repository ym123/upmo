package logger

import (
	"fmt"
	"runtime"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

// Error logs the error
func Error(d *discordgo.Session, guildID string, err error) {
	if db == nil {
		return
	}

	var s Settings

	if err := node.One("GuildID", guildID, &s); err != nil {
		return
	}

	if _, err := d.State.Channel(s.LogChannel); err != nil {
		node.DeleteStruct(&s)
	}

	var traces = make([]string, 0, 3)

	for i := 1; i < 4; i++ {
		p, file, ln, ok := runtime.Caller(i)
		if !ok {
			break
		}

		d := runtime.FuncForPC(p)
		fileparts := strings.Split(file, "/")

		traces = append(traces, fmt.Sprintf(
			"%s#L%d: %s()\n",
			fileparts[len(fileparts)-1], ln, d.Name(),
		))
	}

	e := &discordgo.MessageEmbed{
		Color:       0xFF0000,
		Title:       err.Error(),
		Description: "```" + strings.Join(traces, "\n") + "```",
		Timestamp:   time.Now().Format(time.RFC3339),
	}

	d.ChannelMessageSendEmbed(s.LogChannel, e)
}
