package playground

import (
	"gitlab.com/diamondburned/upmo/playground/bash"
	"gitlab.com/diamondburned/upmo/playground/golang"
	"gitlab.com/diamondburned/upmo/playground/rust"
)

// Language is the type for all languages
type Language string

const (
	Go    Language = "go"
	Rust  Language = "rust"
	V     Language = "v"
	Bash  Language = "bash"
	Shell Language = "sh"
)

// Languages contains all languages
var Languages = []Language{
	Go, Rust, Bash, Shell,
}

// ReflectLanguage reflects the language string for a Playground
func ReflectLanguage(l Language) Playground {
	switch l {
	case Go:
		return &golang.Golang{}
	case Rust:
		return &rust.Rust{}
	case Bash, Shell:
		return &bash.Bash{}
	default:
		return nil
	}
}
