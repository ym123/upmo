package playground

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/diamondburned/upmo/playground/golang"
)

func TestExecute(t *testing.T) {
	s, err := Execute(&golang.Golang{}, ` :import "fmt"
 :import "time"
fmt.Println(time.Now().Format(time.RFC3339))`, true)

	spew.Dump(s, err)
}
