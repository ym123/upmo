package tags

import (
	"path/filepath"

	"github.com/Necroforger/dgrouter/exrouter"
	"github.com/asdine/storm"
)

var (
	db *storm.DB

	singletonError error
)

// Initialize sets up the Tags module.
func Initialize(directory string, stormOptions ...func(*storm.Options) error) {
	db, singletonError = storm.Open(
		filepath.Join(directory, "tags.db"),
		stormOptions...,
	)
}

// Check checks for error
func Check(h exrouter.HandlerFunc) exrouter.HandlerFunc {
	return func(ctx *exrouter.Context) {
		if singletonError != nil {
			ctx.Reply("Error initializing: " + singletonError.Error())
			return
		}

		if db == nil {
			ctx.Reply("Database not initialized.")
			return
		}

		h(ctx)
	}
}
