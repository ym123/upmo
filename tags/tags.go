package tags

import (
	"time"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

// Tag contains a tag. The methods provided follow the
// CRUD interface.
type Tag struct {
	Name        string `storm:"id"`
	Content     string
	Attachments []string // List of URLs

	// Author ID
	Author    string
	Timestamp time.Time
}

// AddTag adds a tag for a guild.
func AddTag(g *discordgo.Guild, t *Tag) error {
	return tx(g.ID, func(n storm.Node) error {
		return n.Save(t)
	})
}

// GetTag gets a tag for a guild.
func GetTag(g *discordgo.Guild, name string) (*Tag, error) {
	var t Tag

	if err := tx(g.ID, func(n storm.Node) error {
		return n.One("Name", name, &t)
	}); err != nil {
		return nil, err
	}

	return &t, nil
}

// RemoveTag removes a tag for a guild.
func RemoveTag(g *discordgo.Guild, name string) error {
	return tx(g.ID, func(n storm.Node) error {
		var t Tag
		if err := n.One("Name", name, &t); err != nil {
			return err
		}

		return n.DeleteStruct(&t)
	})
}
