package main

import (
	"strings"

	"github.com/bwmarrin/discordgo"
)

var (
	markdownEscaper = strings.NewReplacer(
		"*", "\\*",
		"_", "\\_",
		"~", "\\~",
		"`", "\\`",
	)

	everyoneEscaper = strings.NewReplacer(
		"@everyone", "@\u200be\u200bver\u200byo\u200bne",
		"@here", "@\u200bh\u200ber\u200be",
	)
)

func fmtUsername(m *discordgo.Member) string {
	s := &strings.Builder{}

	s.WriteString(
		m.User.Username + "#" + m.User.Discriminator,
	)

	if m.Nick != "" {
		s.WriteByte(' ')

		s.WriteString(
			"(" + m.Nick + ")",
		)
	}

	return s.String()
}
