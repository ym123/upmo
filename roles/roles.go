package roles

import (
	"errors"

	"github.com/asdine/storm"
	"github.com/bwmarrin/discordgo"
)

var (
	errRoleNotFound = errors.New("Role not found")
)

// ResetAll resets all the regions in the database to nothing
func ResetAll(d *discordgo.Session, guildID string) error {
	node, err := node.Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := node.Drop(guildID); err != nil {
		return err
	}

	return node.Commit()
}

// SplitRegion sets the role as a region separator
func SplitRegion(d *discordgo.Session, guildID string, r *discordgo.Role) error {
	return updateNode(guildID, func(gdb storm.Node) error {
		return gdb.Save(&Region{
			StartID: r.ID,
		})
	})
}

// SetAllowGive sets whether or not the entire region is user-obtainable
func SetAllowGive(d *discordgo.Session, guildID string, r *discordgo.Role, give bool) error {
	return updateNode(guildID, func(gdb storm.Node) error {
		var rg Region
		if err := gdb.One("StartID", r.ID, &rg); err != nil {
			return err
		}

		rg.AllowGive = give

		return gdb.Save(&rg)
	})
}

// SetString sets whether or not the entire region is user-obtainable. This
// is mainly used to pretty-print the roles, and would be the default role
// name if this is empty.
func SetString(d *discordgo.Session, guildID string, r *discordgo.Role, s string) error {
	return updateNode(guildID, func(gdb storm.Node) error {
		var rg Region
		if err := gdb.One("StartID", r.ID, &rg); err != nil {
			return err
		}

		rg.String = s

		return gdb.Save(&rg)
	})
}

func updateNode(guildID string, f func(node storm.Node) error) error {
	node, err := node.From(guildID).Begin(true)
	if err != nil {
		return err
	}

	defer node.Rollback()

	if err := f(node); err != nil {
		return err
	}

	return node.Commit()
}
