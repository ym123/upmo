package smartlink

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/diamondburned/upmo/logger"
)

// Handler handles incoming messages and parses them
func Handler(d *discordgo.Session, m *discordgo.MessageCreate) {
	url := ParseURL(strings.Fields(m.Content)[0])
	if url != nil {
		tm, err := d.State.Message(url.ChannelID, url.MessageID)
		if err != nil {
			tm, err = d.ChannelMessage(url.ChannelID, url.MessageID)
			if err != nil {
				return
			}
		}

		var name = tm.Author.Username

		if m, err := d.State.Member(tm.ChannelID, tm.Author.ID); err == nil {
			if m.Nick != "" {
				name += " (" + m.Nick + ")"
			}
		}

		e := &discordgo.MessageEmbed{
			Author: &discordgo.MessageEmbedAuthor{
				Name:    name,
				IconURL: tm.Author.AvatarURL("32"),
			},
			Description: tm.Content,
		}

		if len(tm.Attachments) > 0 {
			a := tm.Attachments[0]

			if a.Width > 0 && a.Height > 0 {
				e.Image = &discordgo.MessageEmbedImage{
					URL:      a.URL,
					ProxyURL: a.ProxyURL,
					Width:    a.Width,
					Height:   a.Height,
				}
			}
		}

		if _, err := d.ChannelMessageSendEmbed(m.ChannelID, e); err != nil {
			logger.Error(d, m.GuildID, err)
		}
	}
}
